%% This function will be called each time a new round starts. 
% You need to fill out transactions array and return the state. 
% Just modify the code startign from the marked line
function [state, transactions] = jh_bot_example_make_move(state)
    % Initialize transactions cell array. All you need todo to make a
    % move is to fill this array
    
    transactions = {};
    if state.round > 1 
        % MODIFY AFTER THIS LINE
        % -----------------------------------------------------------------
        % state.data.users contains information about users - id, name,
        % tokens recevied from them on previous round.
        % It is a cell array that can be accessed using {} brackets
        
        % To keep history or any other VARIABLES that you would like to access
        % later, put them into state.local struct. 
        % For example state.local.foo = 'bar'. It will be accessable during
        % next move, for example like this disp(state.local.foo)

    
        % This variable indicates number of available tokens for current
        % round
        availableTokens = state.data.availableToks;
        
        for i= 1:length(state.data.users)
            % To make is comformtable lets have a separate user object
            % during each iteration. USERS ARRAY INCLUDES YOUR PLAYER TOO.
            
            user = state.data.users{i};
            
            % user.isCurrentPlayer will indicate your player
            fprintf('Recevied %d from user %s with id %s\n', user.received, user.name, user.id);
            
            % Lets play tit for tat and send same amount of tokens recevied
            % from a user back.
            if (availableTokens > user.received)
                amountToSend = user.received;
            else 
                amountToSend = availableTokens;
            end
            availableTokens = availableTokens - amountToSend;
            
            % Now add a strcut that indicates a single transaction to the
            % transactions cell array. dst field indicates the id of the
            % receving user and amountToSend indicates the amount of tokens
            % to be sent to the user
            transactions{end+1} = struct('dst', user.id, 'amount', amountToSend);
        end
        % STOP MODIFYING AFTER THIS LINE
        %------------------------------------------------------------------
    end
    
    
end