
%% Returns api object functions 
% *This function must be first called to obtain the api*
function retFuns = jh_api(stateObj)  

    retFuns.getActiveGames    = @getActiveGames;
    retFuns.joinGame          = @joinGame;
    retFuns.startGame         = @startGame;
    retFuns.createGame        = @createGame;
    retFuns.leaveGame         = @leaveGame;
    retFuns.getGameState      = @getGameState;
    retFuns.makeTransaction   = @makeTransaction;
   
    
end

%% Cleans up the karma
function newStateObj = cleanRes(state)
    newStateObj = state;
    if isfield(newStateObj, 'res')
        newStateObj = rmfield(newStateObj, 'res');
    end
    if isfield(newStateObj, 'err')
        newStateObj = rmfield(newStateObj, 'err');
    end
    if isfield(newStateObj, 'isGood')
        newStateObj = rmfield(newStateObj, 'isGood');
    end
end

%% Gets active games from the server
% Gets active games from the server
function newStateObj = getActiveGames(stateObj)
    newStateObj = stateObj;
    newStateObj= cleanRes(newStateObj);
    % Create headers
    
    headers = [
        http_createHeader('authorization',sprintf('Bearer %s', stateObj.token))
    ];
   
    % Make the reqeust to the server
    [output, response] = urlread2(sprintf('http://%s/json/games', stateObj.serverHost), 'GET', '', headers);
    % if all is good
    if response.isGood
        % Parse the json 
        json = loadjson(output);
        newStateObj.isGood = 1;
        % If server completed the action sucessfully 
        if strcmp(json.res, 'OK')
            newStateObj = catstruct(newStateObj, json);
            newStateObj.games = json.data;
        else
            fprintf('Something went wrong while gettign active games. Server response err:%s \n', json.err);
        end 
    else
        % Something went wrong
        newStateObj.isGood = 0;
        disp(output);
        error('Something went wrong. Server response ');
    end
end


%% Join an existing game
% gameId - id of the game to join
function newStateObj = joinGame(stateObj, gameId)
    newStateObj = stateObj;
    newStateObj= cleanRes(newStateObj);
    headers = [
        http_createHeader('authorization',sprintf('Bearer %s', stateObj.token)),
        http_createHeader('Content-Type', 'application/json')
    ];

    [output, response] = urlread2(sprintf('http://%s/json/games/%d/join', stateObj.serverHost, gameId), 'POST', '', headers);
    json = struct();
    try
        json = loadjson(output);
    catch err
        json.res = 'NOK';
        json.err = 'Unable to parse json from the server. Please check the connection';
    end
    
    newStateObj = catstruct(newStateObj, json);
    
    if response.isGood
        newStateObj.isGood = 1;
        if strcmp(json.res, 'OK')
            % If everything went ok, then set the gameId in the state
            % object and set round to 0
            newStateObj = catstruct(newStateObj, json);

            newStateObj.gameId = gameId;
            newStateObj.round = 0;
            
        else
            fprintf('Something went wrong. Server response err:%s \n', json.err);
        end 
    else
        newStateObj.isGood = 0;
        disp('Something went wrong. Server response ');
        disp(output);
    end
end

%% Start existing game
% gameId - id of the game to start
function newStateObj = startGame(stateObj, gameId)
    newStateObj = stateObj;
    newStateObj= cleanRes(newStateObj);
    headers = [
        http_createHeader('authorization',sprintf('Bearer %s', stateObj.token)),
        http_createHeader('Content-Type', 'application/json')
    ];

    [output, response] = urlread2(sprintf('http://%s/json/games/%d/start', stateObj.serverHost, gameId), 'POST', '', headers);
    json = struct();
    try
        json = loadjson(output);
    catch err
        json.res = 'NOK';
        json.err = 'Unable to parse json from the server. Please check the connection';
    end

    newStateObj = catstruct(newStateObj, json);

    if response.isGood
        newStateObj.isGood = 1;
        if strcmp(json.res, 'OK')
            % If everything went ok, then set the gameId in the state
            % object and set round to 0
            newStateObj = catstruct(newStateObj, json);

            newStateObj.gameId = gameId;
            newStateObj.round = 0;

        else
            fprintf('Something went wrong. Server response err:%s \n', json.err);
        end
    else
        newStateObj.isGood = 0;
        disp('Something went wrong. Server response ');
        disp(output);
    end
end


%% Leave a joined game an existing game
% gameId - id of the game to join
function newStateObj = leaveGame(stateObj, gameId)
    newStateObj = stateObj;
    newStateObj= cleanRes(newStateObj);
    headers = [
        http_createHeader('authorization',sprintf('Bearer %s', stateObj.token)),
        http_createHeader('Content-Type', 'application/json')
    ];

    [output, response] = urlread2(sprintf('http://%s/json/games/%d/leave', stateObj.serverHost, gameId), 'POST', '', headers);
    json = loadjson(output);
    newStateObj = catstruct(newStateObj, json);
    
    if response.isGood
        
        newStateObj.isGood = 1;
        newStateObj = catstruct(newStateObj, json);
        if strcmp(json.res, 'OK')
            if isfield(newStateObj, 'gameId')
                newStateObj = rmfield(newStateObj, 'gameId');
                newStateObj.round = 0;
            end
        else
            fprintf('Something went wrong. Server response err:%s \n', json.err);
        end 
    else
        newStateObj.isGood = 0;
        disp('Something went wrong. Server response ');
        disp(output);
    end
end

%% Make a move in the game
function newStateObj = createGame(stateObj, name, maxPlayersNumber, maxRoundsNumber, maxRoundTimeInSeconds)
    newStateObj = stateObj;
    newStateObj= cleanRes(newStateObj);
    
    formData = {};
    formData.name = name;
    formData.maxplayers = maxPlayersNumber;
    formData.maxrounds = maxRoundsNumber;
    formData.timeout = maxRoundTimeInSeconds; 
    
    
    headers = [
        http_createHeader('authorization',sprintf('Bearer %s', stateObj.token));
        http_createHeader('Content-Type', 'application/json');
    ];
    jsonData = savejson('', formData);
    fprintf('Posting %s\n', jsonData);

    [output, response] = urlread2(sprintf('http://%s/json/games', stateObj.serverHost), 'POST', jsonData, headers);
    disp(output);
    if response.isGood == 1 
        json = loadjson(output);
        newStateObj = catstruct(newStateObj, json);
        newStateObj.isGood = 1;
        if strcmp(json.res, 'OK')
            newStateObj = catstruct(newStateObj, json);
            newStateObj.gameId = json.game; 
        else
            fprintf('Something went wrong. Server response err:%s \n', json.err);
        end 
    else
        newStateObj.isGood = 0;
        disp('Something went wrong. Server response ');
        disp(output);
    end

end




%% Get game state
function newStateObj = getGameState(stateObj)
    newStateObj = stateObj;
    newStateObj= cleanRes(newStateObj);
    if ~isfield(stateObj, 'gameId')
        newStateObj.isGood = 0;
        disp('No gameId field in stateObj was found. Please join a game and pass new state object to this function');
    else     
        headers = [
            http_createHeader('authorization',sprintf('Bearer %s', stateObj.token));
            http_createHeader('Content-Type', 'application/json');
        ];
        
        url = sprintf('http://%s/json/games/%d/state/%d', stateObj.serverHost, stateObj.gameId, stateObj.round);
        
        [output, response] = urlread2(url, 'GET', '', headers);

        if response.isGood 
            json = loadjson(output);
            newStateObj.isGood = 1;
            newStateObj = catstruct(newStateObj, json);
            if strcmp(json.res, 'OK')
                newStateObj = catstruct(newStateObj, json.data);
            else
                fprintf('Something went wrong. Server response err:%s \n', json.err);
            end 
        else
            newStateObj.isGood = 0;
            disp('Something went wrong. Server response ');
            disp(output);
        end
    end
end


%% Make a move in the game
function newStateObj = makeTransaction(stateObj, transCellArr)
    newStateObj = stateObj;
    newStateObj= cleanRes(newStateObj);
    if ~isfield(stateObj, 'gameId')
        newStateObj.isGood = 0;
        disp('No gameId field in stateObj was found. Please join a game and pass new state object to this function');
    else     
        headers = [
            http_createHeader('authorization',sprintf('Bearer %s', stateObj.token));
            http_createHeader('Content-Type', 'application/json');
        ];
        jsonData = savejson('', transCellArr);
        fprintf('Posting %s\n', jsonData);
        
        [output, response] = urlread2(sprintf('http://%s/json/games/%d/transactions/%d', stateObj.serverHost, stateObj.gameId, stateObj.round), 'POST', jsonData, headers);
        disp(output);
        if response.isGood == 1 
            json = loadjson(output);
            newStateObj = catstruct(newStateObj, json);
            newStateObj.isGood = 1;
            if strcmp(json.res, 'OK')
                newStateObj = catstruct(newStateObj, json);
            else
                fprintf('Something went wrong. Server response err:%s \n', json.err);
            end 
        else
            newStateObj.isGood = 0;
            disp('Something went wrong. Server response ');
            disp(output);
        end
    end
end


