%% Create a state struct which contains settings and current game state
% due to functional nature of matlab you must provide state object to
% each jh api function. 
% You can read more about using struct in matlab via googling 'matlab struct' 
state = struct();
state.local = struct();

%% Set the settings, i.e. your token - which can be obtained from
% www.juniorhighgame.com -> Login -> Go to settings at top right side -> get
% api key
state.token = 'YOUR_TOKEN_HERE';

% Set the server host, most probably it must be set to
% www.juniorhighgame.com
state.serverHost = 'www.juniorhighgame.com';
%state.serverHost = 'localhost:5000';

%% Start the program via initializing the bot api
jh = jh_api(state);

%% Now lets get the list of current active games 
% Here is how you call a fucntion. All JH functions return the new state 
% after calling them with the old state. So the results of the action will be 
% available in the new state variable 
state = jh.getActiveGames(state);

%% If the operation was completed successfuly then state.res will be equal to 'OK'
% otherwise it will be 'NOK' and state.err will contain the error message

% Lets check if the operation of getting games finished successfuly 
if(strcmp(state.res, 'NOK')) 
    % If it not, then quit with the error message received from the server
    error(state.err); 
end

%% after getActiveGames call, state.games will contain a strcut of available
% games. We are interested in game ids which are used to join a game. 
% length(state.games) will be the number of available games. Lets list them
if isfield(state, 'games') && length(state.games) > 0 
    disp('Available games are');
    for i = 1 : length(state.games)
        fprintf('%s  id:%d\n', state.games{i}.name, state.games{i}.id)
    end
    state = jh.joinGame(state, state.games{1}.id);
else
    disp('No games found, creating one');
    state = jh.createGame(state, 'It`s Alive!', 10, 25, 90);
    state = jh.joinGame(state, state.game);
end

%% Lets join the first game in the list. You may put any logic for choosing
% a game to join
% GAME JOINING PART IS HERE!



%% Lets check if the operation of joining game was ok 
if(strcmp(state.res, 'NOK')) 
    % If it not, then quit with the error message received from the server
    disp('Unable to join the game');
    error(state.err); 
end


fprintf('Joined game lets start waiting for the start\n');

%% Now this is an endless cycle to wait game to start. 
waiting = true;
refreshPause = 3;

numberOfUsersNeededToStart = 2;
while waiting
    state = jh.getGameState(state);
    % If state is 0 then game has not sta
    if state.state == 0
       fprintf('Game %d has not started yet, waiting for another %d seconds\n', state.gameId, refreshPause)  
       
       if length(state.data.users) >= numberOfUsersNeededToStart
            state = jh.startGame(state, state.gameId);
            waiting = false;
       end
       pause(refreshPause);
    end
    % If state is 1 then the game has started
    if state.state == 1
       fprintf('Game %d has started!\n', state.gameId)  
       waiting = false;
    end
end

fprintf('Starting main game cycle\n');
waiting = true;
lastRound = -1;
while waiting
    state = jh.getGameState(state);
    % If state is 1 then the game has started and in progress
    if state.state == 1 && lastRound < state.round
        fprintf('------------------\nNew round %d\n', state.round');
        % Invokes make move function, feel free to write your own
        [state, transactions] = jh_bot_example_make_move(state);
        % make move, post transacitons to the server
        state = jh.makeTransaction(state, transactions);
        % Set current round
        lastRound = state.round;
    end
    
    if state.state == 2
        fprintf('The game has ended');
        waiting = false;
        break;
    end
    
    pause(refreshPause);
    
end

disp(state);
